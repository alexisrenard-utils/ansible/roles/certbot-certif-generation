# Ansible Role: Automatic certbot certification


Generate and renew automatically a certbot certificate on a webserver. Use case example : enable HTTPS traffic with the new SSL certificate. 
## Usage

* Clone the project and its submodule(s)
```bash
git clone --recurse-submodules <this repo>
```
* Initiate the submodule(s) recursively - the right way
```bash
git submodule update --init --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```
* You're ready to go !

☝️ *git version 2.17.1*

## Variables:

* **`certbot_email`**:

  * Default : `renard.alexis@hotmail.fr`
  * Description : ``



* **`domain_name`**:

  * Default : `renardalexis.com`
  * Description : ``



* **`domain_name_www`**:

  * Default : `www.renardalexis.com`
  * Description : ``


## Tags:

* `certbot`

## License
Project under the
 [MIT](https://tldrlegal.com/license/mit-license)
 License.

## Appendix
### Working with git submodules

* Add a submodule
```bash
git submodule add <repo_url>
```

* Update the submodules
```bash
git submodule update --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```

* Remove a gitsubmodule in a proper way ([check out the mess over there](https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule/36593218#36593218))
```bash
# Remove the submodule entry from .git/config
git submodule deinit -f path/to/submodule

# Remove the submodule directory from the superproject's .git/modules directory
rm -rf .git/modules/submodule_name

# Remove the entry in .gitmodules and remove the submodule directory located at path/to/submodule
git rm -f path/to/submodule
```

*git version 2.17.1*



## Author Information
This role  was created by: [alexis_renard](https://renardalexis.com)

👉 https://fr.linkedin.com/in/renardalexis


## Automatically generated
Documentation generated using ansible-autodoc : [https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc](https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc)